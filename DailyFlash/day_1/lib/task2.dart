import 'package:flutter/material.dart';

class Task2 extends StatelessWidget {
  const Task2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(builder: (BuildContext context) {
          return const Icon(Icons.ac_unit_outlined);
        }),
        backgroundColor: Colors.redAccent,
        actions: const [
          Icon(Icons.photo_size_select_actual_sharp),
          SizedBox(
            width: 10,
          ),
          Icon(Icons.fast_forward),
          SizedBox(
            width: 10,
          ),
          Icon(Icons.yard_outlined),
          SizedBox(
            width: 10,
          ),
        ],
      ),
    );
  }
}
