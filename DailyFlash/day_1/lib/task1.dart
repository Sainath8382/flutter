import 'package:flutter/material.dart';

class Task1 extends StatelessWidget {
  const Task1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(builder: (context) {
          return const Icon(
            Icons.add_reaction_outlined,
          );
        }),
        title: const Text("App-Bar 1"),
        centerTitle: true,
        actions: const [
          Icon(
            Icons.zoom_out_map_outlined,
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
    );
  }
}
