//import 'package:day_1/task2.dart';
import 'package:day_1/task5.dart';
import 'package:flutter/material.dart';
//import 'package:day_1/task4.dart';
//import 'package:day_1/task1.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Task5(),
    );
  }
}
