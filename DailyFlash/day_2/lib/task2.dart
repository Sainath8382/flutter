import 'package:flutter/material.dart';

class Task2 extends StatelessWidget {
  const Task2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(10),
          height: 100,
          width: 100,
          decoration: const BoxDecoration(
              border: BorderDirectional(
            start: BorderSide(color: Colors.purple, width: 5),
          )),
          child: const Center(
            child: Text("Container-1"),
          ),
        ),
      ),
    );
  }
}
