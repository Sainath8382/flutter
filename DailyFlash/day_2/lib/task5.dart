import 'package:flutter/material.dart';

class DailyFlash05 extends StatefulWidget {
  const DailyFlash05({super.key});

  @override
  State createState() => _DailyFlash05State();
}

class _DailyFlash05State extends State {
  bool boxColor = true;
  bool boxText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("DailyFlash02|5"),
        centerTitle: true,
      ),
      body: Center(
        child: GestureDetector(
          onTap: () {
            setState(() {
              if (boxColor && boxText) {
                boxColor = false;
                boxText = false;
              } else {
                boxColor = true;
                boxText = true;
              }
            });
          },
          child: Container(
            height: 200,
            width: 200,
            color: boxColor ? Colors.red : Colors.blue,
            child: Center(
              child: boxText
                  ? const Text("Click me!")
                  : const Text("Container Tapped"),
            ),
          ),
        ),
      ),
    );
  }
}
