import 'package:flutter/material.dart';

class Task4 extends StatelessWidget {
  const Task4({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(10),
          height: 200,
          width: 400,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
            border: Border.all(color: Colors.red, width: 2.0),
            color: Colors.red.shade200,
          ),
          child: const Center(
            child: Text("Your NAME!!"),
          ),
        ),
      ),
    );
  }
}
