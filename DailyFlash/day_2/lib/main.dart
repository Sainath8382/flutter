import 'package:day_2/task5.dart';
import 'package:flutter/material.dart';
//import 'package:day_2/task1.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: DailyFlash05(),
    );
  }
}
