import 'package:flutter/material.dart';

class Task3 extends StatelessWidget {
  const Task3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(10),
          height: 100,
          width: 100,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topRight: Radius.circular(15),
            ),
            border: Border.all(color: Colors.red, width: 2.0),
          ),
          child: const Center(
            child: Text("Container-1"),
          ),
        ),
      ),
    );
  }
}
