
//Const constructor

class Demo{
	
	final String? str;
	final int? x;
	const Demo(this.str, this.x);

}

void main(){

	Demo obj = const Demo("SAm", 12);
	Demo obj1 = const Demo("SAm", 12);
	
	print(obj.hashCode);
	print(obj1.hashCode);
	
	Demo obj3 = const Demo("SAmir", 12);
	Demo obj4 = const Demo("SAm", 12);
	
	print(obj3.hashCode);
	print(obj4.hashCode);
}
