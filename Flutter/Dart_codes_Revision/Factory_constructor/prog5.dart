
class Demo{

	Demo._private(){

		print("Demo private constructor");
	}
	
	factory Demo(){
		print("In factory constructor");
		return Demo._private();
	}
}




