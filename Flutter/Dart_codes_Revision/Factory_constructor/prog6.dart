
//Realtime example of Factory constructor use

class Backend{

	String? lang;
	
	Backend._private(String lang){

		if(lang == "JavaScript")
			this.lang = "NodeJS";
		else if(lang == "Java")
			this.lang = "SpringBoot";
		else 
			this.lang = "Python";
	}

	factory Backend(String lang){
			
		print("IN factory constructor");
		return Backend._private(lang);
	}
}

	

	
