
class Demo{

	static Demo obj = new Demo();

	factory Demo(){

		print("IN constructor");
		return 10;
	}
	
	Demo fun(){

		return obj;
	}
}

void main(){

	Demo obj = new Demo();
	//obj.fun();
}
