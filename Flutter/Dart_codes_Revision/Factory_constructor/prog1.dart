
//Named constructor

class Demo{

	Demo(){

		print("In normal constructor");
	}

	Demo.one(){

		print("In cons-1");
	}

	Demo.two(){

		print("In cons-2");
	}
}

void main(){

	Demo obj1 = new Demo();
	Demo obj2 = new Demo.one();
	Demo obj3 = new Demo.two();
}
