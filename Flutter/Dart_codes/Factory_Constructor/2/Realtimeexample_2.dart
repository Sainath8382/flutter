
//From an abstract class we can return the object of child class using Factory constructor

abstract class Developer{

	factory Developer(String devType){

		if(devType == 'Backend')
			return Backend();
		else if(devType == 'Frontend')
			return Frontend();
		else if(devType == 'Mobile')
			return Mobile();
		else
			return Other();
	}		
	
	void devLang();
}

class Backend implements Developer{

	void devLang(){

		print("SpringBoot");	
	}
}

class Frontend implements Developer{

	void devLang(){

		print("JS");
	}
}

class Mobile implements Developer{

	void devLang(){

		print("Flutter");	
	}
}

class Other implements Developer{

	void devLang(){

		print("DevOps / Testing");
	}
}


