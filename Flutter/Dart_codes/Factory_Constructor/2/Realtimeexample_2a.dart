
//Realtime example of Factory Constructor including Backend framework along with languages.

import 'Realtimeexample_2.dart';

void main(){

	Developer obj1 = new Developer("Frontend");
	obj1.devLang();

	Developer obj2 = new Developer('Backend');
	obj2.devLang();

	Developer obj3 = new Developer('Mobile');
	obj3.devLang();

	Developer obj4 = new Developer('Coding');
	obj4.devLang();
}
