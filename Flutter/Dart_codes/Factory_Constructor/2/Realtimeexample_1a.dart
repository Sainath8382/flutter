
//Realtime example of Factory Constructor including Backend framework along with languages.

import 'Realtimeexample_1.dart';

void main(){

	Backend obj1 = new Backend("Java");
	obj1.fun();
	Backend obj2 = new Backend('JavaScript');
	obj2.fun();
	Backend obj3 = new Backend('Python');
	obj3.fun();
}
