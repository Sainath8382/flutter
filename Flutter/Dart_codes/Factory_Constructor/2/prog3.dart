
// Returning an object of a private constructor into main function of another file using Factory constructor.

class Demo{

	Demo._private(){

		print("IN demo private constructor");
	}

	factory Demo(){

		print("In Factory constructor");
		return Demo._private();
	}

	void fun(){
		
		print("In fun");
	}
}

