
//Realtime example of Factory Constructor including Backend framework along with languages.

class Backend{

	String? lang;

	Backend._code(String lang){

		if(lang == 'Java')
			this.lang = 'SpringBoot';
		else if(lang == 'JavaScript')
			this.lang = 'NodeJS';
		else
			this.lang = 'SpringBoot/NodeJS';
	}	

	factory Backend(String lang){

		return Backend._code(lang);
	}

	void fun(){

		print(lang);
	}
}
