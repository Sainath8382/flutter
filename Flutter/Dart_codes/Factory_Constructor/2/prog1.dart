
class Demo{

	static Demo obj = new Demo();

	Demo(){

		print("Demo Constructor");
	}

	Demo fun(){

		return obj;
	}
}

void main(){

	Demo obj = new Demo();
	obj.fun();
}
