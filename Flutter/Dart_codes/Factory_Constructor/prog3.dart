
//Named Constructor

class Demo{

	Demo(){

		print("Demo Constructor");
	}

	Demo.one(){

		print("Demo One constructor");
	}

	Demo.two(){

		print("Demo Two Constructor");
	}
}

void main(){

	Demo obj1 = new Demo();
	
	Demo obj2 = new Demo.one();
	Demo obj3 = new Demo.two();
}
