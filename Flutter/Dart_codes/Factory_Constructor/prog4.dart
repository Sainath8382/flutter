
//Constant constructor

class Demo{

	final int? x;
	final String? str;
	
	const Demo(this.x, this.str);
}

void main(){

	Demo obj1 = const Demo(12, "ADAM");
	Demo obj2 = const Demo(12, "ADAM");

	print(obj1.hashCode);
	print(obj2.hashCode);

}
