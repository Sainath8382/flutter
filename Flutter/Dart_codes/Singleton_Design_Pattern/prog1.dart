
//Code for singleton design pattern : This pattern is used for servers . Explanation is that whenever a server is created 
// then it is started only once. Every time an intance is requested the server returns the same object every time. 
// This object is created at once only and then sent to all those who request the same.

//in this way we implement singleton design pattern in java by returning the object through static method in java.

class Demo{

	static Demo obj = new Demo();

	static Demo getObject(){

		return obj;
	}
}

void main(){

	//Demo obj = new Demo();
	print((Demo.getObject()).hashCode);
	print((Demo.getObject()).hashCode);
}
