
//Code for singleton design pattern : This pattern is used for servers . Explanation is that whenever a server is created 
// then it is started only once. Every time an intance is requested the server returns the same object every time. 
// This object is created at once only and then sent to all those who request the same.

//in this way we implement singleton design pattern in java by returning the object through static method in java.

//implementing the same Singleton Design Pattern in Dart using the factory constructor provided by dart.

class Demo{

	static Demo obj = new Demo._private();

	Demo._private(){

		print("Private constructor Demo");
	}

	factory Demo(){

		return obj;
	}
}

void main(){

	Demo obj1 = new Demo();
	Demo obj2 = new Demo();

	print(obj1.hashCode);
	print(obj2.hashCode);
}
