
import 'dart:io';
import 'dart:core';

//Taking user defined input 

void main(){
	
	print("Enter employee-id, name and salary");
	int empid = int.parse(stdin.readLineSync()!);

	String? name = stdin.readLineSync();
	
	double sal = double.parse(stdin.readLineSync()!);

	//print("id = $empid, name = $name, salary = $sal");
	stdout.write("id = $empid, name = $name, salary = $sal");	//This line does not leave a space and the terminal is on the same line

	stdout.write("id = $empid, name = $name, salary = $sal\n");	//This line does the work and is not recommended to use \n in dart.
	
	//To avoid above scenarios use writeln 
	stdout.writeln("id = $empid, name = $name, salary = $sal");	

}
	
