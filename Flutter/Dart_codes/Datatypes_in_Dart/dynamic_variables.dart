
// If the variables are declared as Dynamic then the data which is going to be stored in the variable can also be changed at runtime which is not possible in case of normal variables.
// where error occurs.

void main() {

	dynamic x = 10;
	dynamic y = 20;

	print(x.runtimeType);
	print(y.runtimeType);

	x = 20.5;
	y = 30.5;
	
	print(x.runtimeType);
	print(y.runtimeType);
}
	
