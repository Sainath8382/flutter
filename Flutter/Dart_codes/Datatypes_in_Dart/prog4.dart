
// If at once a variable is of integer datatype having a value assigned and then afterwards if the value is again assigned as a double datatype value then error occurs
//If vice versa then no issues.

// Here there is no error for y = 20.5 because int and double come under num .
main() {

	int x = 10;
	print(x);

	num y = 20;
	print(y);

	y = 20.5;
	print(y);

	x = 30.5;
	print(x);
}
