
//Copy from one file to another

import 'dart:io';

void main(){

	File f1 = new File('abc.txt');
	File f2 = new File('xyz.txt');
	
	f1.create();
	f2.create();

	f1.copySync(f2.path);

	String data = f2.readAsStringSync().substring(0,8);
	print(data);
}
