
// Copying from file to another variable

import 'dart:io';

void main() async{

	File f = new File('C2w.txt');
	
	String str = f.readAsStringSync();
	print(str);

	Future<String> str2 = f.readAsString();
	str2.then((data) => print(data));

	String str3 = await f.readAsString();
	print(str3);
}

	
