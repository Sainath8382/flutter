
//File Handling methods in Dart

import 'dart:io';
import 'dart:async';
import 'dart:core';

void main() async{

	File f = new File("C2w.txt");	

	f.create();	

	print(f.absolute);
	
	print(f.path);

//	print(f.lastAccessedSync());
//	print(f.lastModifiedSync());

	DateTime lastacc = await f.lastAccessed();
	print(lastacc);
	DateTime lastmod = await f.lastModified();
	print(lastmod);

	print(f.lengthSync());

	final data = await f.length();
	print(data);

	final value = f.length();
	value.then((val) => print(val));

	
}
	
