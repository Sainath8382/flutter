
//Unary operator : Performs operations on a single operand . Thus it is termed as Unary operator

void main() {

	int x = 5;
	
	print(++x);	//6
	print(x++);	//6
	print(x);	//7
}
