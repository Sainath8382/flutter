
//Arithmetic Operators in Dart

void main() {

	int x = 10;
	int y = 8;

	print(x+y);
	print(x-y);
	print(x*y);
	print(x/y);
	print(x%y);
	print(x ~/ y);		// Integer division arithmetic operator : it returns the integer value from the division operation which is performed.
}
