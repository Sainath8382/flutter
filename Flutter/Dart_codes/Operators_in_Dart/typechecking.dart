
// Type checking operators
// as is is!

void main() {

	int x = 10;
	double y = 20.5;
	num z = 30;

	print(x.runtimeType);	//int
	print(y.runtimeType);	//double
	print(z.runtimeType);	//int 

	print(x is int);	//true
	print(x is double);	//false
	print(x is! double);	//true
//	print( x is y);		//error: y is not a type
	print(x is num);	//true

	print(y is int);	//false
	print(y is num);	//true

	print(z is int);	//true
	print(z is num);
}
	
