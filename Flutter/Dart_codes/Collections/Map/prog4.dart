
//SplayTreeMap : sorts the entered data

import 'dart:collection';

void main(){

	var player = SplayTreeMap();
	player[18] = "Virat";

	player.addAll({45: "Rohit", 1: "KLRahul"});
	print(player);

	player.addEntries({ 96 :"Iyer", 77 : "Shubman"}.entries);
	print(player);

	player.update(18,(value) => "Virat");
	print(player);
/*
	//UnModifiableMapBase

	var player1 = UnmodifiableMapBase(player);
*/

	//UnmodifiableMapView
	
	var player2 = UnmodifiableMapView(player);
	player2[7] = "MSD";

	print(player2);
}
