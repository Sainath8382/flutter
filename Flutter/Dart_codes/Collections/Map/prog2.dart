//Adding data to a map

import 'dart:collection';

void main(){

	//way1
	var player = HashMap();
	player[18] = "Virat";

	print(player);

	player.addAll({45:"Rohit"});
	print(player);
	
	player.addEntries({7:"MSD",1:"KLRahul",/*7:"MSD"*/}.entries);
	print(player);
}
