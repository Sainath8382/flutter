
//constructors in list.

//code for empty constructor.

void main(){

	List player1 = List.empty();	//Fixed Length list
	List player2 = List.empty(growable:true);	//Growable list

	//player1.add("Rohit");
	//player1[0] ="Rohit";

	//player2[0] = "Virat";		error: valid value range is empty 0

	print(player1);
	print(player2);

	player2.add("MSD");
	player2.add( "Rohit");

	print(player2);
	
	player2[1] = "KL";
	print(player2);

}
