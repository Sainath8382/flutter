
//unmodifiable constructor in List.

void main(){

	List player1 = ["Virat", "MSD", "Raina"];
	List player2 = List.unmodifiable(player1);
	
	print(player1);
	print(player2);

	player1[0] = "KLRahul";
	print(player1);
	print(player2);

	player2[2] = "DK";
	print(player2);	
}
