
void main(){

	List Lang = List.empty(growable:true);

	Lang.add("CPP");
	Lang.add("Java");
	Lang.add("Python");
	Lang.add("Java");

	print(Lang);
	print(Lang[3]);
		
	print(Lang.elementAt(3));	//Java
	print(Lang.getRange(0,2));
	print(Lang.lastIndexOf("Java"));	//3
	print(Lang.lastIndexOf("C"));	//-1
	
	print(Lang.indexWhere((element)=> element.startsWith("P")));
	
	print(Lang.indexWhere((element) => element.startsWith("J")));
}
