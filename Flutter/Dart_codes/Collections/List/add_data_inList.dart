
//Adding elements in a List

void main(){

	var progLang = List.empty(growable: true);

	progLang.add("CPP");
	progLang.add("Java");
	progLang.add("Python");
	progLang.add("Java");
	
	print(progLang);

	progLang.insert(3, "Dart");
	print(progLang);

	progLang.insertAll(4, ["SpringBoot", "ReactJS", "JS"]);
	print(progLang);

	progLang.replaceRange( 3,7,["DART", "Swift"]);     
	print(progLang);
}
