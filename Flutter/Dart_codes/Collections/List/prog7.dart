
//Properties of List

void main(){

	List player = ["Virat", "MSD", "Rohit", "DK"];

	print(player);
	print(player.first);
	print(player.hashCode);
	print(player[0].hashCode);
	print(player.isEmpty);
	print(player.isNotEmpty);
	print(player.last);
	print(player.length);
	print(player.reversed);
	print(player.runtimeType);
	
	List player1 = ["MSD"];
	print(player1.single);
}
