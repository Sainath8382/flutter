
import "dart:collection";

final class Company extends LinkedListEntry<Company>{

	int empCount;
	String compName;
	double rev;

	Company(this.empCount, this.compName, this.rev);

	String toString(){

		return "$empCount $compName $rev";
	}
}

void main(){

	var compInfo = LinkedList<Company>();

	compInfo.add(new Company(1000, "Veritas", 100.00));
	compInfo.add(new Company(2000, "Google", 521.00));
	compInfo.add(new Company(5500, "Apple", 600.00));
	
	print(compInfo);
	
	compInfo.last.unlink();

	print(compInfo);
}
