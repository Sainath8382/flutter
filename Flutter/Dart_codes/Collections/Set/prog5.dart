
//Unmodifiable Set

import "dart:collection";

void main(){

	var lang = LinkedHashSet();
	lang.add("Java");
	lang.add("CPP");
	lang.add("Dart");

	print(lang);

	var progLang = UnmodifiableSetView(lang);
	progLang.add("Flutter");
	print(progLang);
}

