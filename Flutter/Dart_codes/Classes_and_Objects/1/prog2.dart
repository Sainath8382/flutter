
//User Input for the same.

import 'dart:io';

class Employee{

	int? eid ;
	String? eName ;
	double? sal ;

	void empInfo(){
		
		stdout.writeln("Id : $eid, Name : $eName, Sal : $sal");
	}
}

void main(){

	Employee obj = new Employee();
	print("Enter id");
	
	obj.eid = int.parse(stdin.readLineSync()!);
	
	print("Enter name");
	obj.eName = stdin.readLineSync();

	print("Enter salary");
	obj.sal = double.parse(stdin.readLineSync()!);

	obj.empInfo();
}
