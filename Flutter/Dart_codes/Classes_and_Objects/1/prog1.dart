
//Class named cric-Player

class Player{

	int jerNo = 7;
	String pName = "MSD";

	void playerInfo(){
		
		print("Jersey : $jerNo");
		print("Name : $pName");
	}
}

void main(){

	Player obj = new Player();
	obj.playerInfo();

	//Changing the data wrt to perspective of another object.
	Player obj1 = new Player();
	obj1.jerNo = 10;
	obj1.pName = "Sachin";

	obj1.playerInfo();
		
	Player obj2 = new Player();

	obj2.playerInfo();
}
