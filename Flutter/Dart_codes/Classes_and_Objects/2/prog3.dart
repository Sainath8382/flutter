
class Company{

	int empCount = 500;
	String compName = "Google";
	String loc = "Pune";

	void compInfo(){

		print(empCount);
		print(compName);
		print(loc);
	}
}

void main(){
	
	//4 different ways of declaring the objects in Dart for a class.

	Company obj = new Company();	//This is the recommended way to do so as we can tell the object is created in Runtime Mempry Area.
	obj.compInfo();

	Company obj2 = Company();
	obj2.compInfo();

	//The both ways down here can be used for one time use only.
	new Company().compInfo();

	Company().compInfo();	//This seems to be call for a function named Company() in the code But here object is created.
}
