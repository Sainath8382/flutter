
class Employee{

	int eid = 10;
	String name = "Sam";
	double sal = 1.56;

	void empInfo(){

		print(eid);
		print(name);
		print(sal);
	}
}

void main(){

	Employee obj1 = new Employee();
	obj1.empInfo();

	Employee obj2 = new Employee();
	obj2.empInfo();

}
