//Constructor

class Demo{

	int x = 10;
	
	void printData(){

		print(x);
	}

	Demo(int a){

		print("Demo constructor");
		
		x = a;

		print(x);
	}
/*
	void printData(){

		print(x);
	}*/
}

void main(){

	Demo obj = new Demo(200);
	obj.printData();	
}
