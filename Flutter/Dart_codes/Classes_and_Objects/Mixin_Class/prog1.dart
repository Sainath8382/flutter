
abstract class IFC{

	void material(){
		print("Indian Material");
	}

	void taste();
}

class IndianFC implements IFC{

	void material(){

		print("Indian MAterial");
	}

	void taste(){

		print("Indian Taste");
	}
}

class EUFC implements IFC{

	void material(){

		print("EU material");
	}

	void taste(){

		print("Indian Taste");
	}
}

void main(){

	IndianFC obj = new IndianFC();
	obj.material();
	obj.taste();
}
