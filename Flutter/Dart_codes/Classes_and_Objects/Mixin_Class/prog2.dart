
//Multiple Inheritance in Java

abstract class InterfaceDemo1{

	void m1(){

		print("In m1 Demo1");
	}

}

abstract class InterfaceDemo2{

	void m2(){

		print("In m2 Demo");
	}
}

class Demo implements InterfaceDemo1, InterfaceDemo2{

	void m1(){
		print("in m1");
	}

	void m2(){

		print("IN m2");
	}
}

void main(){

}
