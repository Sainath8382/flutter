
class Demo{

	int? x;
	String? str;

	void Data(){

		print(x);
		print(str);
	}
}

void main(){

	Demo obj = new Demo();
	
	obj.Data();

	print('-------------');	
	obj.x = 10;
	obj.str = "Kanha";
	
	obj.Data();
}
