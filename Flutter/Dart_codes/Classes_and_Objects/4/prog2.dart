
class Demo{

	int x = 10;
	static int y = 20;

	static void Data(){	
		print(x);	//Here all the static is already present and since x is a instance variable it has no memory if you dont create object
				//Thus there is error for x as undefined.
				//Also if method is not static then there is no error in this code.
		print(y);
	}
}

void main(){

}
