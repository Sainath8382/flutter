
//Parameterized Constructor

class Demo{

	int? x;
	String? str;

	Demo(int x, String str){

		print("Parameterized constructor");
		
		print(x.hashCode);

		//print(x.identityHashCode());

		this.x = x;
		this.str = str;
	}

	void Data(){

		print(x);
		print(str);
		print(x.hashCode);
	}
}

void main(){

	Demo obj = new Demo(10, "Sam");
	obj.Data();
}
