
// If 2 objects have the same data and they are declared constant then they get the same space.

class Demo{

	final int? x;
	final String? str;

	const Demo(this.x, this.str);
}

void main(){

	Demo obj1 = const Demo(10, "SAM");
	print(obj1.hashCode);

	Demo obj2 = const Demo(10, "SAM");
	print(obj2.hashCode);
}
