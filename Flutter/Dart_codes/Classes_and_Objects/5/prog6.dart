
//2. Parameterized constructor

class Employee{

	int? empid;
	String? empName;

	Employee(this.empid, this.empName);

}

void main(){

	Employee obj = new Employee(10, "Ashish");
}
