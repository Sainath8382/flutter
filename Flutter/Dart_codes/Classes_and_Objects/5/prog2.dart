
class Company{

	int? empCount;
	String? compName;

	Company(this.empCount, [this.compName = "Amdocs"]);	// Here the second parameter is optional i.e. if any value is given then it is used and if not given then the optional value itself is used.

	void compInfo(){
		
		print(empCount);
		print(compName);	
	}

}

void main(){

	Company obj1 = new Company(100, "Veritas");
	obj1.compInfo();

	Company obj2 = new Company(115);
	obj2.compInfo();
}
	
