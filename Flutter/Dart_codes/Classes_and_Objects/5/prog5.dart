//Types of constructor

//1. Default constructor

class Employee{
	
	int? empId;
	String? empName;

	Employee(){

		print("Default Parameter");
	}
}

void main(){

	Employee obj = new Employee();
}
