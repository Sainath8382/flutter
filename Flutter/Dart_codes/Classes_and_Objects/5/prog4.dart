
//4th way Named Parameter

class Company{

	int? empCount;
	String? compName;

	Company({this.empCount, this.compName});

	void compInfo(){

		print(empCount);
		print(compName);
	}
}

void main(){

	Company obj1 = new Company(empCount: 100, compName: "Veritas");
	obj1.compInfo();

	Company obj2 = new Company(compName: "UST", empCount: 120);
	obj2.compInfo();
}
	
