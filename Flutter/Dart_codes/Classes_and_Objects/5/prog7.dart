
//Named Constructor Type.

class Employee{

	int? empid;
	String? ename;

	Employee(){

		print(" Default Constructor");
	}

	Employee.construct(int empid, String ename){
		
		print("Parameterized constructor");
	}
}

void main(){

	Employee obj1 = new Employee();
	Employee obj2 = new Employee.construct(10,"Sam");
}
