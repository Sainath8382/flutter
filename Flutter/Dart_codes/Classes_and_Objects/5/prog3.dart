
class Company{

	int? empCount;
	String? compName;

//constructor way 3

	Company(this.empCount, {this.compName = "Amdocs"});	// Here the second parameter is default i.e. if any value is passed there then it shows error.The default value should not be changed.

	void compInfo(){
		
		print(empCount);
		print(compName);	
	}

}

void main(){

	Company obj1 = new Company(100);
	obj1.compInfo();

	Company obj2 = new Company(115);
	obj2.compInfo();
}
	
