
//Overloading means 2 methods in a same class having different number of parameters. In dart it is not supported as methods are also considered as objects in dart and thus the same object cannot have different properties.

class Parent{

	int x = 10;
	int y = 20;

	void disp(int x){

		this.x = x;
	}

	void disp(int x, int y){

	}
}

void main(){

	Parent obj = new Parent();
	obj.disp(10);
	obj.disp(10,20);
}
	
