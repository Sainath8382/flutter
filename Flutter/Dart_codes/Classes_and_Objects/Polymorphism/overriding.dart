
class Parent{

	void career(){

		print("Engg");
	}

	void marry(){

		print("Iulia Vantur");
	}
}

class Child extends Parent{

	void marry(){

		print("Shraddha Kapoor");
	}
}

void main(){

	Child obj = new Child();
	obj.career();	
	obj.marry();
}
