
import 'prog7.dart';
	
void main(){
	
	Demo obj1 = new Demo(10, "Kanha", 1.5);
	obj1.disp();
/*	
	obj1.setX(15);
	obj1.setName("Sham");	// Explicitly : manual call
	obj1.setSal(2.56);
*/

	obj1.setX = 15;
	obj1.setName = "Sam";	// Implicitly : automatic call
	obj1.setSal = 5.0;
		
	obj1.disp();

}
