
//Getter method way1

class Demo{

	int? _x;
	String? str;
	double? _sal;

	Demo(this._x, this.str, this._sal);

	int? getX(){
		
		return _x;	//the variables are private thus getter method is required.
	}	

	double? getSal(){

		return _sal;
	}
}



