
//Setter All Methods

class Demo{

	int? _x;
	String? str;
	double? _sal;

	Demo(this._x, this.str, this._sal);

/*
	//Setter Way 1

	void setX(int x){

		_x = x;
	}
	
	void setName(String name){

		str = name;
	}

	void setSal(double sal){

		_sal = sal;
	}
*/
/*
	//Setter Way 2
	
	set setX(int x){

		_x = x;
	}

	set setName(String name){
		
		str = name;
	}

	set setSal(double sal){
		
		_sal = sal;
	}
*/


	//Setter Way 3
	
	set setX(int x) => _x = x;
	set setName(String name) => str = name;
	set setSal(double sal) => _sal = sal;

	void disp(){
		print(_x);
		print(str);
		print(_sal);
	}
}


		
