
class Parent{

	Parent(){

		print("Parent Constructor");
	}

	call(){
		print("In method Call");
	}
}

class Child extends Parent{

	Child(){

		super();
		print("Child constructor");
	}
}

void main(){

	Child obj = new Child();
	obj();
}
