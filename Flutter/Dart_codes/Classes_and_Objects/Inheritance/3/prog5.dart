
//imp code

class Company{

	String? compName;
	String? loc;

	Company(this.compName, this.loc);
	
	void dispData(){

		print(compName);
		print(loc);
	}
}

class Employee extends Company{

	int? empId;
	String? name;

	Employee(this.empId, this.name, String compName, String loc) : super(compName, loc);
	
	void Data(){

		print(empId);
		print(name);
	}	
}

void main(){

	Employee obj = new Employee(15, "Kanha", "Veritas", "Baner");
	obj.Data();
	obj.dispData();
}
