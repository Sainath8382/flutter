
class Parent{

	int x = 10;
	Parent(){
		print("Parent this = $this.hashCode");
		print("Parent Constructor");
	}

	void printData(){

		print(x);	
	}
}

class Child extends Parent{

	int x = 20;
	Child(){

		print("Child constructor");	
		print("Child this = $this.hashCode");
	}
	
	void dispData(){

		print(x);	
	}
}

void main(){

	Child obj = new Child();
	obj.dispData();
	obj.printData();
}
