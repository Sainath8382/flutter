
//Passing arguments to Child Constructor

class Parent{

	int? x;
	String? str;

	Parent(this.x, this.str);

	void printData(){

		print(x);
		print(str);
	}
}

class Child extends Parent{
	
	int? y;
	String? name;

	Child(this.y, this.name);
	
	void dispData(){

		print(y);
		print(name);
	}
}

void main(){

	Child obj = new Child(10, "Messi");
}
