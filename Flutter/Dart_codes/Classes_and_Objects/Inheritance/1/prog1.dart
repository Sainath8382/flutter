
class Parent{

	int x = 10;
	String str = "Sam";

	void ParentDisp(){

		print(x);
		print(str);
	}
}

class Child extends Parent{

}

void main(){

	Child obj = new Child();
	print(obj.x);
	print(obj.str);
	
	obj.ParentDisp();
}
