
class ICC{

	int noofTeams = 10;
	String loc = "London";
	
	void disp(){

		print("Teams = $noofTeams");
		print("Location = $loc");
	}
}

class BCCI extends ICC{

	int noPlayers = 45;
	String loc = "Mumbai";

	void disp(){

		print("Players = $noPlayers");
		print("Location = $loc");
	}
}

void main(){
	
	BCCI obj = new BCCI();
	obj.disp();
	
	ICC obj1 = new ICC();
	obj1.disp();
}
