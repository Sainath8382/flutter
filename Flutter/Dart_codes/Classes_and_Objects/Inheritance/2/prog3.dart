
class Parent{

	int x =10;
	String str1 = "name";
	
	void parentInfo(){

		print(x);
		print(str1);
	}
}

class Child extends Parent{

	int x = 20;
	String str1 = "Data";
	
	void childInfo(){

		print(x);
		print(str1);
	}
}

void main(){

	Child obj1 = new Child();
	print(obj1.x);			//20
	print(obj1.str1);		//Data
	obj1.parentInfo();		//20 Data -- the this here is of child this the output is like this. 
	
	print(obj1.x);			//20
	print(obj1.str1);		//Data
	obj1.childInfo();		// 20 Data
}
	
