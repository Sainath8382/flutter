
class Parent{

	int x =10;
	String str1 = "name";
	
	void parentInfo(){

		print(x);
		print(str1);
	}
}

class Child extends Parent{

	int x = 20;
	String str1 = "Data";
	
	void childInfo(){

		print(x);
		print(str1);
	}
}

void main(){

	Parent obj1 = new Parent();
	Child obj2 = new Child();

	print(obj2.x);			//20
	print(obj2.str1);		//Data
	obj2.parentInfo();		//20 Data
	
	obj1.parentInfo();		//10 name
}
	
