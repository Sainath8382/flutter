
//Realtime example of Abstract classes

abstract class Developer{

	void develop(){
		print("Software Developer");
	}

	void devType();
}

class webDev extends Developer{
	
	void devType(){
	
		print("Frontend");
	}
}

class MobileDev extends Developer{
	
	void devType(){
		print("Flutter");

	}
}

void main(){

	Developer obj1 = new webDev();
	obj1.develop();
	obj1.devType();
	
	Developer obj2 = new MobileDev();
	obj2.develop();
	obj2.devType();

/*
	Developer obj3 = new Developer();
	obj3.develop();
	obj3.devType();
*/
}
