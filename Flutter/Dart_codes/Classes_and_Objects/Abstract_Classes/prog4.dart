
//My realtime example of Abstract class

abstract class TataGroup{

	int? founYear;

	TataGroup(this.founYear);
	
	void OrgInfo(){

		print("Tata Group founded : $founYear");
	}

	void Ceo();
}

class Titan extends TataGroup{

	Titan(int founYear):super(founYear);

	void Ceo(){

		print("Titan : N Chandrasekaran");
	}
}

class TCS extends TataGroup{

	TCS(int founYear):super(founYear);	

	void Ceo(){

		print("TCS : Ratan Tata Sir");
	}
}

void main(){

	TataGroup obj1 = new Titan(1968);
	obj1.OrgInfo();
	obj1.Ceo();
	
	TataGroup obj2 = new TCS(1968);
	obj2.OrgInfo();
	obj2.Ceo();
}
	
