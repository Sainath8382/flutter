
abstract class Demo{

	Demo(){

		print("In Demo Cons");
	}

	void fun1(){

		print("In fun1");
	}

	void fun2();
}

class DemoChild extends Demo{

	DemoChild(){

		print("In DemoCHild cons");
	}

	void fun2(){

		print("In fun2");
	}
}

void main(){

	Demo obj = new DemoChild();
	obj.fun1();
	obj.fun2();
}
