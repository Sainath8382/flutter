
//Abstract Class

abstract class Parent{

	void property(){

		print("Flat, Gold , Car");
	}

	void career();
	void marry();
}

class Child extends Parent{

	void career(){

		print("Engg");
	}
	
	void marry(){
		print("Disha PAtni");
	}
}

void main(){

	Parent obj = new Child();
	obj.property();
	obj.career();
	obj.marry();
}

	
