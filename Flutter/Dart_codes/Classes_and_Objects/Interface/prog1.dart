
abstract class Developer{

	void develop(){

		print("We Build Softwares");
	}

	void devType();
}

class MobileDev implements Developer{

	void develop(){

		print("We Build Softwares-Coding,etc");
	}

	void devType(){

		print("Flutter dev");
	}
}

void main(){

	Developer obj = new MobileDev();
	obj.develop();
	obj.devType();

//	Developer obj1 = new Developer();	cannot be instantiated.

}
