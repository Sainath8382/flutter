
abstract class Developer{

	int x =10;

	Developer(){

		print("Developer");
	}

	void develop(){

		print("Software Developer");
	}

	void devType();
}

class MobileDev implements Developer{

	int x = 50;

	MobileDeveloper(){

		print("Mobile Developers");
	}

	void develop(){

		print("Mobile app developer");	
	}
	
	void devType(){

		print("FLutter dev");
	}
}

void main(){

	Developer obj = new MobileDev();
	obj.develop();
	obj.devType();
}



