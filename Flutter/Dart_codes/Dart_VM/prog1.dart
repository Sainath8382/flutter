
void main(){

	int num = 1234;

	int sum = 0;
	while(num != 0){

		int val = num%10;
		
		if(val %2 ==0)
			sum = sum+val;
		
		num = num ~/ 10;
	}

	print(sum);
}
