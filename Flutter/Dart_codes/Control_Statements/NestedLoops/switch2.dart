
// In other programming languages if a case is true and break is not added in all of the cases then all the cases after the true statement get executed.
// Whereas in dart the break is not necessary to add to make dart easy break is automotically present.

void main(){

	int x = 1;
	
	switch(x){
		
		case 1:
			print("One");
		case 2:
			print("Two");
		case 3:
			print("Three");
		case 4:
			print("Four");
		default:
			print("No Match");
	}
}
