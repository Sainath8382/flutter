
// Named Argument : A concept where if the arguments are switched with each other while passing them to the function fun the arguments reach the matching parameter.

//void fun(String name , double sal){
//void fun({String name , double sal}){

void fun({String? name , double? sal}){		//The above both lines give error this is the correct way of Named Argument.

	print('In fun');
	print(name);
	print(sal);
}

void main(){
	
	print("Start Main");
	fun( sal : 20.5 ,name : "Kanha");
	print("End main");
}


