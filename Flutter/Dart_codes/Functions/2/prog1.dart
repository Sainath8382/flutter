
void playerInfo({ int? jerNo, String? name}){	// The ? signifies an extra datatype in dart int? meaning int nullable  
						// String? means String nullable. i.e. this nullable datatypes mean that the value can be null or value can be given both work!!

	print(jerNo);
	print(name);
}

void main(){

	playerInfo(name: "MSD", jerNo : 7);
	playerInfo(name: "Rohit");
	playerInfo(jerNo : 18);

}
