
//Required Keyword : If required keyword is present then the nullable datatype needs the value null is not allowed

//void playerInfo(String team , {int? jerNo, String? name}){

void playerInfo(String team , {required int? jerNo,required String? name}){

	print(team);
	print(jerNo);
	print(name);
}

void main(){

	playerInfo("India");
	playerInfo("India", jerNo: 18);
	playerInfo("India", jerNo: 18, name: "Virat");
}
