
// Printing the multiplication of numbers in a series using Recursion
//This is the same code for factorial of a given number.

int mul = 1;

int mult(int x){
	
	if(x == 1)
		return 1;

	mul = x * mult(--x);
	return mul;

}

void main(){
	
	mult(5);
	print(mul);
}
