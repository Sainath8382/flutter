
// Addition of a series of numbers using Recursion

int sum = 0;

int fun(int x){

	if(x == 0)
		return 0;
		
	sum = x + fun(--x);

	return sum;
}

void main(){

	fun(5);
	print(sum);

}
	
