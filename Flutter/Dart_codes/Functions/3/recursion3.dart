
//print numbers in range using Recursion without changing function prototype.

//int i = 1;

void fun(){

	static int i=1;
	
	if(i > 10){
		return;
	}

	print(i);
	i++;
	fun();
}

void main(){

	fun();
}
