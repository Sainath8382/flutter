
Future<String> getOrder(){

	return Future.delayed(Duration(seconds: 5), ()=>"BUrger");
}

Future<String> getOrderMessage() async{

	var order = await getOrder();
	return "Your order is $order";
}

Future<void> main() async{	// here void main() also works directly.

	print("Start");
	print(await getOrderMessage());
	print("End");
}
