import 'package:flutter/material.dart';
import 'package:assignment_4/assignment1.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment1(),
      title: "Stateful_1",
    );
  }
}
