//Widget Life-Cycle (Stateful Widget) internal code.

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: WidgetLifeCycle(),
    );
  }
}

class WidgetLifeCycle extends StatefulWidget {
  const WidgetLifeCycle({super.key});

  @override
  State createState() => _WidgetLifeCycleState();
}

class _WidgetLifeCycleState extends State {
  @override
  void initState() {
    print("In init state");
    super.initState();
    didChangeDependencies();
  }

  @override
  void didChangeDependencies() {
    print("In didChangeDependencies");
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(StatefulWidget oldWidget) {
    print("In didUpdateWidget");
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    print("In build");
    onUserInteracts();
    return const Scaffold();
  }

  //SetState()
  void onUserInteracts() {
    setState(() {});
  }

  @override
  void deactivate() {
    print("In deactivate");
    super.deactivate();
    dispose();
  }

  @override
  void dispose() {
    print("In dispose");
    super.dispose();
  }
}
