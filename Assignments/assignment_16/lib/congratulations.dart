import 'package:flutter/material.dart';

class Congratulations extends StatefulWidget {
  const Congratulations({super.key});

  @override
  State<StatefulWidget> createState() => _Congratulations();
}

class _Congratulations extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Image.network(
              "https://imgs.search.brave.com/RjWslDHyfnxM2rABya1XbpQq6brOjxUB3ydwywb8oAs/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9pbWcu/ZnJlZXBpay5jb20v/ZnJlZS1waG90by9o/YW5kcy1ob2xkaW5n/LXVwLXZpY3Rvcnkt/dHJvcGh5XzkxMTI4/LTM4NjkuanBnP3Np/emU9NjI2JmV4dD1q/cGc"),
          const Center(
            child: Text(
              'Congratulations',
              style: TextStyle(fontSize: 30),
            ),
          ),
        ],
      ),
    );
  }
}
