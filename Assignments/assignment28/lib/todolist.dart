import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class ToDoList extends StatefulWidget {
  const ToDoList({super.key});

  @override
  State createState() => _ToDoListState();
}

class ToDoModelClass {
  String title;
  String taskinfo;
  String date;

  ToDoModelClass(
      {required this.title, required this.taskinfo, required this.date});
}

class _ToDoListState extends State {
  List cardColors = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),
  ];
/*
  @override
  void dispose() {
    super.dispose();
    _titleController.dispose();
    _dateController.dispose();
    _descController.dispose();
  }
*/
  List<ToDoModelClass> tasks = [
    ToDoModelClass(
        title: "Demo Task Card",
        taskinfo: "Refer this card for demo of TO-Do APP",
        date: "10 January 2024"),
  ];

  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descController = TextEditingController();
  final TextEditingController _dateController = TextEditingController();

  void deletetask(ToDoModelClass toDoModelobj) {
    setState(() {
      tasks.remove(toDoModelobj);
    });
  }

  void editTasks(ToDoModelClass? toDoModelobj) {
    _titleController.text = toDoModelobj!.title;
    _descController.text = toDoModelobj.taskinfo;
    _dateController.text = toDoModelobj.date;

    bottomSheet(true, toDoModelobj);
  }

  void submit(bool edit, [ToDoModelClass? toDoModelobj]) {
    if (_titleController.text.trim().isNotEmpty &&
        _descController.text
            .trim()
            .isNotEmpty && //To avoid submitting empty cards this condition is important
        _dateController.text.trim().isNotEmpty) {
      if (!edit) {
        //To avoid submitting empty cards this condition is important
        setState(() {
          tasks.add(ToDoModelClass(
            title: _titleController.text.trim(),
            taskinfo: _descController.text.trim(),
            date: _dateController.text.trim(),
          ));
        });
      } else {
        //If not add new card then simply set the same previous text to the controllers.
        setState(() {
          toDoModelobj!.title = _titleController.text.trim();
          toDoModelobj.taskinfo = _descController.text.trim();
          toDoModelobj.date = _dateController.text.trim();
        });
      }
    }
    clearControllers(); //After submit is clicked then the text fields should be emptied.
  }

  void clearControllers() {
    _titleController.clear();
    _descController.clear();
    _dateController.clear();
  }

  void bottomSheet(bool edit, [ToDoModelClass? toDoModelobj]) {
    //bool edit = false;

    showModalBottomSheet(
        isDismissible: true,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
              //Avoid keyboard overlap
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "Create Task",
                  style: GoogleFonts.quicksand(
                    textStyle: const TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(
                      width: 15,
                    ),
                    Text(
                      "Title",
                      style: GoogleFonts.quicksand(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    TextField(
                      scrollPadding: const EdgeInsets.only(left: 15),
                      controller: _titleController,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Text("Description",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(0, 139, 148, 1),
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                        )),
                    const SizedBox(
                      height: 3,
                    ),
                    TextField(
                      controller: _descController,
                      maxLines: 4,
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Text(
                      "Date",
                      style: GoogleFonts.quicksand(
                        color: const Color.fromRGBO(0, 139, 148, 1),
                        fontWeight: FontWeight.w400,
                        fontSize: 15,
                      ),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    TextField(
                      controller: _dateController,
                      readOnly: true,
                      decoration: InputDecoration(
                        suffixIcon: const Icon(Icons.date_range_rounded),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                      ),
                      onTap: () async {
                        DateTime? pickeddate = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2024),
                            lastDate: DateTime(2030));

                        String formatedDate =
                            DateFormat.yMMMd().format(pickeddate!);
                        setState(() {
                          _dateController.text = formatedDate;
                        });
                      },
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 50,
                  width: 300,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                    onPressed: () {
                      if (edit == true) {
                        submit(edit, toDoModelobj);
                      } else {
                        submit(edit);
                      }
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "Submit",
                      style: GoogleFonts.inter(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: const Text(
          "ADD-YOUR-PROJECTS",
        ),
        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
      ),
      body: ListView.builder(
        shrinkWrap: true,
        itemCount: tasks.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 25,
              horizontal: 15,
            ),
            child: Container(
              padding: const EdgeInsets.all(10),
              //height: 112,
              //width: 330,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.1),
                    blurRadius: 20,
                    offset: Offset(0, 10),
                    spreadRadius: 1,
                  ),
                ],
                color: cardColors[index % cardColors.length],
              ),
              child: Column(children: [
                Row(
                  children: [
                    Container(
                      height: 52,
                      width: 52,
                      decoration: const BoxDecoration(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        shape: BoxShape.circle,
                      ),
                      child: Image.network(
                        "https://cdn-icons-png.flaticon.com/128/1375/1375157.png",
                        height: 19.07,
                        width: 23.79,
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          SizedBox(
                            height: 15,
                            width: 243,
                            child: Text(
                              tasks[index].title,
                              //"Lorem Ipsum is simply setting industry.",
                              style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 12,
                              )),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            width: 243,
                            height: 44,
                            child: Text(
                              tasks[index].taskinfo,
                              //"Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
                              style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 10,
                              )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 14,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  child: Row(
                    children: [
                      Text(tasks[index].date,
                          style: GoogleFonts.quicksand(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: const Color.fromRGBO(132, 132, 132, 1),
                          )),
                      const Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () {
                              editTasks(tasks[index]);
                            },
                            child: const Icon(
                              Icons.edit_outlined,
                              color: Color.fromRGBO(0, 139, 148, 1),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          GestureDetector(
                            onTap: () {
                              deletetask(tasks[index]);
                            },
                            child: const Icon(Icons.delete_outline,
                                color: Color.fromRGBO(0, 139, 148, 1)),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ]),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          clearControllers();
          bottomSheet(false);
        },
        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
        child: const Icon(
          Icons.add,
        ),
      ),
    );
  }
}
