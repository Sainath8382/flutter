import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyAppDemo(),
    );
  }
}

class TaskList {
  String? heading;
  String? task;
  String? date;
  TaskList({this.heading, this.task, this.date});
}

class MyAppDemo extends StatefulWidget {
  const MyAppDemo({Key? key}) : super(key: key);

  @override
  State createState() => _MyAppState();
}

class _MyAppState extends State<MyAppDemo> {
  List<Color> colorList = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),
  ];

  List<TaskList> allTasks = [];

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  bool isEdit = true;
  int titleIdx = -1;

  Widget _bottomField(context) {
    if (isEdit == false) {
      titleController.text = allTasks[titleIdx].heading!;
      descriptionController.text = allTasks[titleIdx].task!;
      dateController.text = allTasks[titleIdx].date!;
    }
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text(
              "Create State",
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 22,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            TextField(
              controller: titleController,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  labelText: "Title"),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              controller: descriptionController,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  labelText: "Description"),
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              controller: dateController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                labelText: "Date",
                suffixIcon: Icon(Icons.calendar_month_outlined),
              ),
              readOnly: true,
              onTap: () async {
                DateTime? pickedDate = await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(2024),
                    lastDate: DateTime(2025));
                if (pickedDate != null) {
                  String formattedDate = DateFormat.yMMMd().format(pickedDate);
                  setState(() {
                    dateController.text = formattedDate;
                  });
                }
              },
            ),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    if (isEdit == false) {
                      if (titleController.text.isNotEmpty &&
                          descriptionController.text.isNotEmpty &&
                          dateController.text.isNotEmpty) {
                        allTasks[titleIdx].heading = titleController.text;
                        allTasks[titleIdx].task = descriptionController.text;
                        allTasks[titleIdx].date = dateController.text;
                        titleController.clear();
                        descriptionController.clear();
                        dateController.clear();
                        Navigator.pop(context);
                      }
                    } else if (titleController.text.isNotEmpty &&
                        descriptionController.text.isNotEmpty &&
                        dateController.text.isNotEmpty) {
                      allTasks.add(TaskList(
                          heading: titleController.text,
                          task: descriptionController.text,
                          date: dateController.text));
                      titleController.clear();
                      descriptionController.clear();
                      dateController.clear();
                      Navigator.pop(context);
                    }
                  });
                },
                child: const Text("Submit"))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "To-do-list",
          style: TextStyle(fontWeight: FontWeight.w700, fontSize: 23),
        ),
      ),
      body: ListView.builder(
        shrinkWrap: true,
        itemCount: allTasks.length,
        itemBuilder: (context, index) {
          return Container(
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              boxShadow: const [
                BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.07), blurRadius: 10)
              ],
              borderRadius: BorderRadius.circular(10),
              color: colorList[(index) % (colorList.length)],
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      height: 52,
                      width: 52,
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.07),
                                blurRadius: 10)
                          ]),
                      child: Image.asset(
                        "assets/Group 42.png",
                        height: 19,
                        width: 23,
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${allTasks[index].heading}",
                            style: const TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 13,
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            "${allTasks[index].task}",
                            style: const TextStyle(fontSize: 13),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text("${allTasks[index].date}"),
                    const Spacer(),
                    GestureDetector(
                      child: const Icon(
                        Icons.edit_outlined,
                        color: Color.fromRGBO(2, 167, 177, 1),
                      ),
                      onTap: () {
                        setState(() {
                          isEdit = false;
                          titleIdx = index;
                          showModalBottomSheet(
                              isScrollControlled: true,
                              context: context,
                              builder: (BuildContext context) {
                                return _bottomField(context);
                              });
                        });
                      },
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      child: const Icon(
                        Icons.delete_outline,
                        color: Color.fromRGBO(2, 167, 177, 1),
                      ),
                      onTap: () {
                        setState(() {
                          allTasks.removeAt(index);
                        });
                      },
                    )
                  ],
                ),
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
              isScrollControlled: true,
              context: context,
              builder: (BuildContext context) {
                return _bottomField(context);
              });
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
