import 'package:flutter/material.dart';
//import 'package:google_fonts/google_fonts.dart';
import 'package:assignment28/todolist.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: //ToDoList()
          ToDoList(),
      debugShowCheckedModeBanner: false,
    );
  }
}

/*
void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ToDo(),
    );
  }
}

class ToDo extends StatefulWidget {
  const ToDo({super.key});

  @override
  State<ToDo> createState() => _ToDoState();
}

class _ToDoState extends State<ToDo> {
  List<Color> colorList = const [
    Color.fromRGBO(250, 232, 232, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1)
  ];

  List<TaskModal> taskList = [];

  DateTime? date;
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  void takeInput(bool editFlag, {int index = -1}) {
    if (editFlag) {
      titleController = TextEditingController(text: taskList[index].title);
      descriptionController =
          TextEditingController(text: taskList[index].description);
      dateController = TextEditingController(text: taskList[index].date);
    } else {
      titleController.clear();
      descriptionController.clear();
      dateController.clear();
    }

    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.only(
            left: 30,
            right: 30,
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    (!editFlag) ? "Create Task" : "Edit Task",
                    style: GoogleFonts.quicksand(
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Text(
                    "Title",
                    style: GoogleFonts.quicksand(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    height: 50,
                    width: 350,
                    alignment: Alignment.center,
                    child: TextField(
                      controller: titleController,
                      style: GoogleFonts.quicksand(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: const Color.fromRGBO(0, 0, 0, 0.7)),
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(0, 139, 148, 1)))),
                    ),
                  )
                ],
              ),
              Row(
                children: [
                  Text(
                    "Description",
                    style: GoogleFonts.quicksand(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    height: 72,
                    width: 350,
                    alignment: Alignment.center,
                    child: TextField(
                      controller: descriptionController,
                      style: GoogleFonts.quicksand(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: const Color.fromRGBO(0, 0, 0, 0.7)),
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(0, 139, 148, 1)))),
                    ),
                  )
                ],
              ),
              Row(
                children: [
                  Text(
                    "Date",
                    style: GoogleFonts.quicksand(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    height: 50,
                    width: 350,
                    alignment: Alignment.center,
                    child: TextField(
                      controller: dateController,
                      style: GoogleFonts.quicksand(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: const Color.fromRGBO(0, 0, 0, 0.7)),
                      readOnly: true,
                      decoration: const InputDecoration(
                          suffixIcon: Icon(
                            Icons.calendar_month_outlined,
                            size: 20,
                          ),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(0, 139, 148, 1)))),
                      onTap: () async {
                        date = await showDatePicker(
                            context: context,
                            firstDate: DateTime.now(),
                            initialDate: DateTime(2023),
                            lastDate: DateTime(2040));
                      },
                    ),
                  )
                ],
              ),
              const SizedBox(height: 20),
              Container(
                height: 50,
                width: 300,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color.fromRGBO(0, 139, 148, 1),
                ),
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context);
                      if (!editFlag) {
                        taskList.add(TaskModal(
                            title: titleController.text,
                            description: descriptionController.text,
                            date: "${date!.day}-${date!.month}-${date!.year}"));
                      } else {
                        taskList[index] = TaskModal(
                            title: titleController.text,
                            description: descriptionController.text,
                            date: "${date!.day}-${date!.month}-${date!.year}");
                      }
                    });
                  },
                  style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(
                          Color.fromRGBO(0, 139, 148, 1))),
                  child: Text(
                    "Submit",
                    style: GoogleFonts.inter(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        color: Colors.white),
                  ),
                ),
              ),
              const SizedBox(height: 20),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "To-do list",
          style: GoogleFonts.quicksand(
              fontWeight: FontWeight.w700,
              height: 18,
              fontSize: 26,
              color: Colors.white),
        ),
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
      ),
      body: Padding(
        padding:
            //const EdgeInsets.only(top: 30, left: 15, right: 15, bottom: 40),
            const EdgeInsets.symmetric(horizontal: 15, vertical: 35),
        child: ListView.builder(
          itemCount: taskList.length,
          itemBuilder: (context, index) {
            return Column(children: [
              Container(
                  decoration: BoxDecoration(
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.black38,
                          offset: Offset(5, 5),
                          blurRadius: 1)
                    ],
                    color: colorList[index % colorList.length],
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Row(
                            children: [
                              Container(
                                height: 52,
                                width: 52,
                                //padding: const EdgeInsets.all(10),
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle),
                                child: Image.asset('assets/Vector.png'),
                              ),
                              Expanded(
                                // Expanded automatically
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 15,
                                      width: 243,
                                      child: Text(
                                        taskList[index].title,
                                        style: GoogleFonts.quicksand(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.black),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    SizedBox(
                                      height: 44,
                                      width: 243,
                                      child: Text(
                                        taskList[index].description,
                                        style: GoogleFonts.quicksand(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                taskList[index].date,
                                style: GoogleFonts.quicksand(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: const Color.fromRGBO(132, 132, 132, 1),
                                ),
                              ),
                              const Spacer(
                                  // flex: 50,   Nahi dila tari chalta
                                  ),
                              //Edit icon button
                              GestureDetector(
                                onTap: () {
                                  takeInput(true, index: index);
                                },
                                child: const Icon(
                                  size: 25,
                                  Icons.edit,
                                ),
                              ),
                              const SizedBox(
                                width: 19,
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    taskList.remove(taskList[index]);
                                  });
                                },
                                child: const Icon(
                                  Icons.delete_outline,
                                  size: 25,
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                            ],
                          ),
                        ]),
                  )),
              const SizedBox(
                height: 20,
              )
            ]);
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          takeInput(false);
        },
        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
        shape: const CircleBorder(side: BorderSide.none),
        child: const SizedBox(
          height: 26,
          width: 26,
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

class TaskModal {
  String title;
  String description;
  String date;

  TaskModal(
      {required this.title, required this.description, required this.date});
}
*/