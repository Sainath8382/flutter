import 'package:flutter/material.dart';

class Palindrome extends StatefulWidget {
  const Palindrome({super.key});

  @override
  State<Palindrome> createState() => _PalindromeState();
}

class _PalindromeState extends State<Palindrome> {
  int palindromecount = 0;
  int armstrongcount = 0;
  int strongcount = 0;

  final List palindromelst = [121, 112, 136, 525];
  final List armstronglst = [153, 1634, 100, 200];
  final List stronglst = [111, 145, 2, 500];

  void palindromecheck() {
    for (int i = 0; i < palindromelst.length; i++) {
      int num = palindromelst[i];
      int temp = num;
      int rem = 0;
      while (temp != 0) {
        rem = rem * 10 + temp % 10;
        temp = temp ~/ 10;
      }

      if (rem == palindromelst[i]) {
        palindromecount++;
      }
    }
  }

  void armstrongcheck() {
    for (int i = 0; i < armstronglst.length; i++) {
      int num = armstronglst[i];
      int temp1 = num;
      int temp2 = num;
      int rem = 0;
      int digit = 0;

      while (temp1 != 0) {
        digit++;
        temp1 = temp1 ~/ 10;
      }

      while (temp2 != 0) {
        int val = temp2 % 10;
        int mul = 1;
        while (digit != 0) {
          mul = mul * val;
        }
        rem = rem * 10 + mul;
        temp2 = temp2 ~/ 10;
      }

      if (rem == num) armstrongcount++;
    }
  }

  void strongcheck() {
    for (int i = 0; i < stronglst.length; i++) {
      int num = stronglst[i];
      int temp = num;
      int rem = 0;

      while (temp != 0) {
        int val = temp % 10;
        int fact = 1;

        while (val != 0) {
          fact = fact * val;
          val--;
        }

        rem = rem * 10 + fact;
        temp = temp ~/ 10;
      }

      if (rem == num) strongcount++;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Number-Checker"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 100,
              width: 100,
              child: Text("$palindromecount"),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  palindromecheck();
                });
              },
              child: const Text("Calculate PalindromeCount"),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 100,
              width: 100,
              child: Text("$armstrongcount"),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  armstrongcheck();
                });
              },
              child: const Text("Calculate ArmstrongCount"),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 100,
              width: 100,
              child: Text("$strongcount"),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  strongcheck();
                });
              },
              child: const Text("Calculate StrongCount"),
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
