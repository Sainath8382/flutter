import 'package:flutter/material.dart';

class Assignment11 extends StatefulWidget {
  const Assignment11({super.key});

  @override
  State<Assignment11> createState() => _Assignment11State();
}

class _Assignment11State extends State<Assignment11> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purpleAccent,
        title: const Text("Padding & Margin"),
      ),
      body: Center(
        child: Container(
          color: Colors.blue,
          child: Container(
            height: 250,
            width: 250,
            color: Colors.amber,
            alignment: Alignment.center,
            padding: const EdgeInsets.all(20),
            margin: const EdgeInsets.all(30),
            child: Image.network(
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3bP38N9ZbRrZyCflLc6OtZx1tzdvfBaxQJw&usqp=CAU"),
          ),
        ),
      ),
    );
  }
}
