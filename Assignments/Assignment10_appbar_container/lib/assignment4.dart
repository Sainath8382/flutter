import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});

  @override
  State<Assignment4> createState() => _Assignment4();
}

class _Assignment4 extends State<Assignment4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Four_APP"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 360,
              height: 200,
              color: Colors.tealAccent,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              width: 360,
              height: 200,
              color: Colors.redAccent,
            ),
          ],
        ),
      ),
    );
  }
}
