import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});

  @override
  State<Assignment1> createState() => _Assignment1();
}

class _Assignment1 extends State<Assignment1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "One-APP",
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: Colors.white,
            fontSize: 15,
          ),
        ),
        backgroundColor: Colors.redAccent,
        actions: const [
          Icon(
            Icons.auto_awesome,
            color: Colors.amber,
            size: 30,
          ),
          Icon(
            Icons.favorite_rounded,
            color: Colors.white,
            size: 30,
          ),
        ],
      ),
    );
  }
}
