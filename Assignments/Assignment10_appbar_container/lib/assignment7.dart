import 'package:flutter/material.dart';

class Assignment7 extends StatefulWidget {
  const Assignment7({super.key});

  @override
  State<Assignment7> createState() => _Assignment7();
}

class _Assignment7 extends State<Assignment7> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Seven_APP"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                'https://img.freepik.com/free-photo/golden-frame-blue-background_53876-92990.jpg?w=360&t=st=1705642293~exp=1705642893~hmac=327e6dd7f8b89efdf81764d7c5887ec6848738d2f3e80003c6d8390dcf524b20',
                height: 200,
                width: 150,
              ),
              const SizedBox(
                width: 1,
              ),
              Image.network(
                'https://img.freepik.com/free-photo/golden-frame-blue-background_53876-92990.jpg?w=360&t=st=1705642293~exp=1705642893~hmac=327e6dd7f8b89efdf81764d7c5887ec6848738d2f3e80003c6d8390dcf524b20',
                height: 200,
                width: 150,
              ),
              const SizedBox(
                width: 1,
              ),
              Image.network(
                'https://img.freepik.com/free-photo/golden-frame-blue-background_53876-92990.jpg?w=360&t=st=1705642293~exp=1705642893~hmac=327e6dd7f8b89efdf81764d7c5887ec6848738d2f3e80003c6d8390dcf524b20',
                height: 200,
                width: 150,
              ),
              const SizedBox(
                width: 2,
              ),
              Image.network(
                'https://img.freepik.com/free-photo/golden-frame-blue-background_53876-92990.jpg?w=360&t=st=1705642293~exp=1705642893~hmac=327e6dd7f8b89efdf81764d7c5887ec6848738d2f3e80003c6d8390dcf524b20',
                height: 200,
                width: 150,
              ),
              const SizedBox(
                width: 2,
              ),
              Image.network(
                'https://img.freepik.com/free-photo/golden-frame-blue-background_53876-92990.jpg?w=360&t=st=1705642293~exp=1705642893~hmac=327e6dd7f8b89efdf81764d7c5887ec6848738d2f3e80003c6d8390dcf524b20',
                height: 200,
                width: 150,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
