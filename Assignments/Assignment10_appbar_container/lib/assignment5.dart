import 'package:flutter/material.dart';

class Assignment5 extends StatefulWidget {
  const Assignment5({super.key});

  @override
  State<Assignment5> createState() => _Assignment5();
}

class _Assignment5 extends State<Assignment5> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Five_APP"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 150,
              width: 150,
              child: Image.asset("asset/1.jpg"),
            ),
            const SizedBox(
              height: 5,
            ),
            SizedBox(
              height: 150,
              width: 150,
              child: Image.asset("asset/2.webp"),
            ),
            const SizedBox(
              height: 5,
            ),
            SizedBox(
              height: 150,
              width: 150,
              child: Image.asset("asset/3.webp"),
            ),
          ],
        ),
      ),
    );
  }
}
