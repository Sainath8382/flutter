import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});

  @override
  State<Assignment2> createState() => _Assignment2();
}

class _Assignment2 extends State<Assignment2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Two_APP"),
        centerTitle: true,
        actions: const [
          Icon(
            Icons.account_balance_sharp,
            size: 30,
          )
        ],
      ),
    );
  }
}
