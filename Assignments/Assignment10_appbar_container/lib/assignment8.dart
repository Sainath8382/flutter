import 'package:flutter/material.dart';

class Assignment8 extends StatefulWidget {
  const Assignment8({super.key});

  @override
  State<Assignment8> createState() => _Assignment8State();
}

class _Assignment8State extends State<Assignment8> {
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: const Text(
            "Border",
            style: TextStyle(
              fontStyle: FontStyle.italic,
              color: Colors.deepPurple,
              fontSize: 30,
            ),
          ),
          actions: const [
            Icon(Icons.favorite_rounded),
            SizedBox(
              width: 10,
            ),
            Icon(Icons.access_alarm_rounded)
          ],
        ),
        body: Center(
            child: SingleChildScrollView(
                child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              color: Colors.red,
            ),
            Container(
              height: 300,
              width: 300,
              color: Colors.blue,
              alignment: Alignment.center,
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.all(20),
            ),
          ],
        ))));
  }
}
