import 'package:flutter/material.dart';

class Assignment6 extends StatefulWidget {
  const Assignment6({super.key});

  @override
  State<Assignment6> createState() => _Assignment6();
}

class _Assignment6 extends State<Assignment6> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Four_APP"),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.tealAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.redAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.tealAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.redAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.tealAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.redAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.tealAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.redAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.tealAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.redAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.tealAccent,
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 360,
                height: 200,
                color: Colors.redAccent,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
