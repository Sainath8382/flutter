import 'package:flutter/material.dart';
import 'package:assignment_5/assignment2.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment2(),
      title: "Stateful_2",
    );
  }
}
