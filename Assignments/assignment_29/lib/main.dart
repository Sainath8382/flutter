import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: TextFieldDemo(),
    );
  }
}

class TextFieldDemo extends StatefulWidget {
  const TextFieldDemo({super.key});

  @override
  State createState() => _TextFieldDemoState();
}

class _TextFieldDemoState extends State {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _compNameController = TextEditingController();
  final TextEditingController _locationController = TextEditingController();
  //final FocusNode _nameFocusNode = FocusNode();

  int submitted = -1;

  void addContainer() {
    data.add(Container(
      margin: const EdgeInsets.all(10),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(
              height: 15,
            ),
            Text(
              "Name : ${_nameController.text}",
              style: const TextStyle(
                fontSize: 25,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              "Company_Name : ${_compNameController.text}",
              style: const TextStyle(
                fontSize: 25,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              "Location : ${_locationController.text}",
              style: const TextStyle(
                fontSize: 25,
              ),
            ),
          ],
        ),
        Column(children: [
          const SizedBox(
            height: 15,
          ),
          Container(
              margin: const EdgeInsets.all(10.0),
              child: Image.network(
                "https://cdn.pixabay.com/photo/2024/01/25/12/30/forest-8531787_1280.jpg",
                height: 100,
                width: 100,
              ))
        ])
      ]),
    ));
  }

  List<Container> data = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Text-Field Demo",
          ),
          backgroundColor: Colors.redAccent,
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            TextField(
              controller: _nameController,
              //focusNode: _nameFocusNode,
              autofocus: true,
              keyboardType: TextInputType.name,
              decoration: const InputDecoration(
                hintText: "Name: ",
                hintStyle: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                  //color: Colors.redAccent,
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.redAccent,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            TextField(
              controller: _compNameController,
              //focusNode: _compNameFocusNode,
              autofocus: true,
              keyboardType: TextInputType.name,
              decoration: const InputDecoration(
                hintText: "Company_Name: ",
                hintStyle: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.redAccent,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            TextField(
              controller: _locationController,
              //focusNode: _compNameFocusNode,
              autofocus: true,
              keyboardType: TextInputType.name,
              decoration: const InputDecoration(
                hintText: "Location: ",
                hintStyle: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.redAccent,
                  ),
                ),
              ),
              //textInputAction: TextInputAction.done,
            ),
            const SizedBox(
              height: 30,
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  addContainer();
                });
              },
              child: const Text("Submit", style: TextStyle(fontSize: 20)),
            ),
            const SizedBox(height: 20),
            const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Dream-Company",
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            SizedBox(
              height: 300,
              child: ListView.builder(
                itemCount: data.length,
                itemBuilder: (context, index) => data[index],
              ),
            ),
          ],
        ));
  }
}
