import 'package:flutter/material.dart';

class Assignment15 extends StatefulWidget {
  const Assignment15({super.key});

  @override
  State<Assignment15> createState() => _Assignment15State();
}

class _Assignment15State extends State<Assignment15> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Portfolio"),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          const SizedBox(
            height: 10,
          ),
          const Text(
            "Name: Sainath Amle",
            style: TextStyle(
              fontSize: 25,
            ),
            textAlign: TextAlign.start,
          ),
          const SizedBox(
            height: 20,
          ),
          Image.asset(
            'assets/myimage.jpg',
            height: 350,
            width: 250,
            fit: BoxFit.contain,
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            "College: RMDSSOE",
            style: TextStyle(
              fontSize: 25,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Image.network(
            "https://rmdstic.sinhgad.edu/wp-content/uploads/2022/07/Sinhgad-Institutes-custom-1024x671.png",
            height: 120,
            width: 150,
            fit: BoxFit.contain,
            colorBlendMode: BlendMode.exclusion,
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            "Dream Company : NVidia",
            style: TextStyle(
              fontSize: 25,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Image.network(
            "https://images7.alphacoders.com/129/1296508.jpg",
            height: 120,
            width: 150,
            fit: BoxFit.contain,
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.next_plan_sharp),
      ),
    );
  }
}
