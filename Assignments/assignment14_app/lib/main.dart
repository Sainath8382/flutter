import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Jai-Hind"),
          backgroundColor: Colors.orange,
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Container(
                  height: 690,
                  width: 20,
                  color: Colors.brown,
                )
              ],
            ),
            Column(
              //mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 40,
                ),
                Container(
                  height: 50,
                  width: 250,
                  color: Colors.orange,
                ),
                Container(
                  height: 50,
                  width: 250,
                  color: Colors.white,
                  child: Center(
                    child: Image.network(
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFEJ8AIEXmQSHKYnZBiv6WX44gH9EDEnOuUoBe5XTqFw&s"),
                  ),
                ),
                Container(
                  height: 50,
                  width: 250,
                  color: Colors.green,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
