import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _namesController = TextEditingController();
  final FocusNode _nameFocusNode = FocusNode();
  List<String> names = [];

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        foregroundColor: Colors.white,
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          TextField(
            controller: _namesController,
            focusNode: _nameFocusNode,
            decoration: InputDecoration(
              hintText: "Enter name",
              border: InputBorder.none,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5.0),
                borderSide: const BorderSide(
                  color: Colors.blue,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: const BorderSide(color: Colors.red),
              ),
            ),
            cursorColor: Colors.amber,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.text,
            onChanged: (value) {
              print("Value = $value");
            },
            onSubmitted: (value) {
              print("DATA Submitted = $value");
            },
          ),
          SizedBox(
              height: 500,
              width: 500,
              child: ListView.builder(
                itemCount: names.length,
                itemBuilder: ((context, index) {
                  return Center(
                    child: Container(
                      height: 50,
                      width: double.infinity,
                      child: Text(names[index]),
                    ),
                  );
                }),
              )),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            names.add(_namesController.text);
          });
        },
        child: const Text("ADD"),
      ),
    );
  }
}
