import 'package:educourse_ui/screen1.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Educourse(),
    );
  }
}

class Educourse extends StatefulWidget {
  const Educourse({super.key});

  @override
  State createState() => EducourseState();
}

class EducourseState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(205, 218, 218, 1),
      body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const SizedBox(height: 47),
        const Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Row(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.menu,
                size: 26.0,
              ),
              Spacer(),
              Icon(
                Icons.notifications_none,
                size: 26.0,
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 19,
        ),
        const Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Text(
            "Welcome to New",
            style: TextStyle(
              fontSize: 26.98,
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Text(
            "Educourse",
            style: TextStyle(
              fontSize: 37,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: TextField(
            decoration: InputDecoration(
              filled: true,
              fillColor: const Color.fromRGBO(255, 255, 255, 1),
              hintText: "Enter your keyword",
              suffixIcon: const Icon(
                Icons.search,
                size: 27.0,
              ),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(25)),
            ),
          ),
        ),
        const SizedBox(
          height: 29,
        ),
        Expanded(
          child: Container(
            padding: const EdgeInsets.only(left: 20, right: 20),
            decoration: const BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 1),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(38),
                    topRight: Radius.circular(38))),
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 33,
                ),
                const Text(
                  "Course For You",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 16,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Screen()),
                          );
                        });
                      },
                      child: Container(
                        height: 242,
                        width: 190,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(14),
                            gradient: const LinearGradient(colors: [
                              Color.fromRGBO(197, 4, 98, 1),
                              Color.fromRGBO(80, 3, 112, 1),
                            ])),
                        child: Column(
                          children: [
                            const Padding(
                              padding:
                                  EdgeInsets.only(top: 20, left: 22, right: 18),
                              child: Text(
                                "UX Designer from Scratch",
                                style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 3,
                            ),
                            Image.asset(
                              "assets/7010826_3255307.png",
                              height: 160,
                              width: 160,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Container(
                      height: 242,
                      width: 190,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                          gradient: const LinearGradient(colors: [
                            Color.fromRGBO(0, 77, 228, 1),
                            Color.fromRGBO(1, 47, 135, 1)
                          ])),
                      child: Column(
                        children: [
                          const Padding(
                            padding:
                                EdgeInsets.only(top: 20, left: 22, right: 18),
                            child: Text(
                              "Design Thinking The Beginner",
                              style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 3,
                          ),
                          Image.asset(
                            "assets/Objects.png",
                            height: 160,
                            width: 160,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Container(
                      height: 242,
                      width: 190,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(14),
                          gradient: const LinearGradient(colors: [
                            Color.fromRGBO(197, 4, 98, 1),
                            Color.fromRGBO(80, 3, 112, 1),
                          ])),
                      child: Column(
                        children: [
                          const Padding(
                            padding:
                                EdgeInsets.only(top: 20, left: 22, right: 18),
                            child: Text(
                              "UX Designer from Scratch",
                              style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 3,
                          ),
                          Image.asset(
                            "assets/7010826_3255307.png",
                            height: 160,
                            width: 160,
                          ),
                        ],
                      ),
                    )
                  ]),
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  "Course By Category",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Container(
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: Color.fromRGBO(25, 0, 56, 1),
                          ),
                          height: 36,
                          width: 36,
                          child: Image.asset(
                            "assets/image1.png",
                            height: 20,
                            width: 20,
                          ),
                        ),
                        const SizedBox(
                          height: 9,
                        ),
                        const Text(
                          "UI/UX",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: Color.fromRGBO(25, 0, 56, 1),
                          ),
                          height: 36,
                          width: 36,
                          child: Image.asset(
                            "assets/image2.png",
                            height: 20,
                            width: 20,
                          ),
                        ),
                        const SizedBox(
                          height: 9,
                        ),
                        const Text(
                          "VISUAL",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: Color.fromRGBO(25, 0, 56, 1),
                          ),
                          height: 36,
                          width: 36,
                          child: Image.asset(
                            "assets/image3.png",
                            height: 20,
                            width: 20,
                          ),
                        ),
                        const SizedBox(
                          height: 9,
                        ),
                        const Text(
                          "ILLUSTRATION",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: Color.fromRGBO(25, 0, 56, 1),
                          ),
                          height: 36,
                          width: 36,
                          child: Image.asset(
                            "assets/image4.png",
                            height: 20,
                            width: 20,
                          ),
                        ),
                        const SizedBox(
                          height: 9,
                        ),
                        const Text(
                          "PHOTO",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        )
      ]),
    );
  }
}
