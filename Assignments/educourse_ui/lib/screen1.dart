import 'package:educourse_ui/main.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Screen(),
    );
  }
}

class Screen extends StatefulWidget {
  const Screen({super.key});

  @override
  State createState() => Screen1();
}

class Screen1 extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [
              Color.fromRGBO(197, 4, 98, 1),
              Color.fromRGBO(80, 3, 112, 1)
            ]),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 47,
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 20,
                ),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Educourse()),
                      );
                    });
                  },
                  child: const Icon(
                    Icons.arrow_back,
                    color: Color.fromRGBO(255, 255, 255, 1),
                  ),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              const Padding(
                padding: EdgeInsets.only(left: 38, right: 44),
                child: Text(
                  "UX Designer from Scratch",
                  style: TextStyle(
                    fontSize: 32.61,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(255, 255, 255, 1),
                  ),
                ),
              ),
              const SizedBox(
                height: 11,
              ),
              const Padding(
                padding: EdgeInsets.only(left: 38, right: 44),
                child: Text(
                  "Basic guideline & tips & tricks for how to become a UX designer easily.",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(228, 205, 225, 1),
                  ),
                ),
              ),
              const SizedBox(
                height: 27,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 37,
                  ),
                  Image.asset(
                    "assets/authoricon.png",
                    height: 34,
                    width: 34,
                  ),
                  const SizedBox(
                    width: 7,
                  ),
                  const Text(
                    "Author:",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: Color.fromRGBO(255, 255, 255, 1)),
                  ),
                  const Text(
                    " Jenny",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: Color.fromRGBO(255, 255, 255, 1)),
                  ),
                  const SizedBox(width: 37),
                  const Text(
                    "4.8",
                    style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 0.8),
                        fontWeight: FontWeight.w500),
                  ),
                  const Icon(
                    Icons.star,
                    color: Color.fromRGBO(255, 146, 0, 1),
                  ),
                  const Text(
                    "(20 review)",
                    style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 0.8),
                        fontSize: 16,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              ),
              const SizedBox(
                height: 32,
              ),
              Expanded(
                child: Container(
                    decoration: const BoxDecoration(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(38),
                          topRight: Radius.circular(38)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(30),
                      child: Column(
                        children: [
                          Row(children: [
                            Column(
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(230, 239, 239, 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(12)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromRGBO(0, 0, 0, 0.15),
                                          offset: Offset(0, 8),
                                          blurRadius: 40,
                                        )
                                      ]),
                                  height: 60,
                                  width: 46,
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: [
                                      //Background Image
                                      Image.asset(
                                        "assets/box.png",
                                        height: 15.46,
                                        width: 22,
                                        //fit: BoxFit.cover,
                                      ),
                                      //Foreground image to be at center of background image
                                      Positioned(
                                          child: Center(
                                        child: Image.asset(
                                          "assets/playbtn.png",
                                          height: 6.54,
                                          width: 5.75,
                                          fit: BoxFit.cover,
                                        ),
                                      )),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(top: 12, bottom: 12, left: 7),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Introduction",
                                      style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(0, 0, 0, 1),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          "Lorem Ipsim is simply dummy text",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Color.fromRGBO(
                                                143, 143, 143, 1),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ]),
                            ),
                          ]),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(children: [
                            Column(
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(230, 239, 239, 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(12)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromRGBO(0, 0, 0, 0.15),
                                          offset: Offset(0, 8),
                                          blurRadius: 40,
                                        )
                                      ]),
                                  height: 60,
                                  width: 46,
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: [
                                      //Background Image
                                      Image.asset(
                                        "assets/box.png",
                                        height: 15.46,
                                        width: 22,
                                        //fit: BoxFit.cover,
                                      ),
                                      //Foreground image to be at center of background image
                                      Positioned(
                                          child: Center(
                                        child: Image.asset(
                                          "assets/playbtn.png",
                                          height: 6.54,
                                          width: 5.75,
                                          fit: BoxFit.cover,
                                        ),
                                      )),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(top: 12, bottom: 12, left: 7),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Introduction",
                                      style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(0, 0, 0, 1),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          "Lorem Ipsim is simply dummy text",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Color.fromRGBO(
                                                143, 143, 143, 1),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ]),
                            ),
                          ]),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(children: [
                            Column(
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(230, 239, 239, 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(12)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromRGBO(0, 0, 0, 0.15),
                                          offset: Offset(0, 8),
                                          blurRadius: 40,
                                        )
                                      ]),
                                  height: 60,
                                  width: 46,
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: [
                                      //Background Image
                                      Image.asset(
                                        "assets/box.png",
                                        height: 15.46,
                                        width: 22,
                                        //fit: BoxFit.cover,
                                      ),
                                      //Foreground image to be at center of background image
                                      Positioned(
                                          child: Center(
                                        child: Image.asset(
                                          "assets/playbtn.png",
                                          height: 6.54,
                                          width: 5.75,
                                          fit: BoxFit.cover,
                                        ),
                                      )),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(top: 12, bottom: 12, left: 7),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Introduction",
                                      style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(0, 0, 0, 1),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          "Lorem Ipsim is simply dummy text",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Color.fromRGBO(
                                                143, 143, 143, 1),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ]),
                            ),
                          ]),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(children: [
                            Column(
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(230, 239, 239, 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(12)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromRGBO(0, 0, 0, 0.15),
                                          offset: Offset(0, 8),
                                          blurRadius: 40,
                                        )
                                      ]),
                                  height: 60,
                                  width: 46,
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: [
                                      //Background Image
                                      Image.asset(
                                        "assets/box.png",
                                        height: 15.46,
                                        width: 22,
                                        //fit: BoxFit.cover,
                                      ),
                                      //Foreground image to be at center of background image
                                      Positioned(
                                          child: Center(
                                        child: Image.asset(
                                          "assets/playbtn.png",
                                          height: 6.54,
                                          width: 5.75,
                                          fit: BoxFit.cover,
                                        ),
                                      )),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(top: 12, bottom: 12, left: 7),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Introduction",
                                      style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(0, 0, 0, 1),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          "Lorem Ipsim is simply dummy text",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Color.fromRGBO(
                                                143, 143, 143, 1),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ]),
                            ),
                          ]),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(children: [
                            Column(
                              children: [
                                Container(
                                  decoration: const BoxDecoration(
                                      color: Color.fromRGBO(230, 239, 239, 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(12)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromRGBO(0, 0, 0, 0.15),
                                          offset: Offset(0, 8),
                                          blurRadius: 40,
                                        )
                                      ]),
                                  height: 60,
                                  width: 46,
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: [
                                      //Background Image
                                      Image.asset(
                                        "assets/box.png",
                                        height: 15.46,
                                        width: 22,
                                        //fit: BoxFit.cover,
                                      ),
                                      //Foreground image to be at center of background image
                                      Positioned(
                                          child: Center(
                                        child: Image.asset(
                                          "assets/playbtn.png",
                                          height: 6.54,
                                          width: 5.75,
                                          fit: BoxFit.cover,
                                        ),
                                      )),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(top: 12, bottom: 12, left: 7),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Introduction",
                                      style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(0, 0, 0, 1),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          "Lorem Ipsim is simply dummy text",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Color.fromRGBO(
                                                143, 143, 143, 1),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ]),
                            ),
                          ]),
                          const SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    )),
              ),
            ],
          )),
    );
  }
}
