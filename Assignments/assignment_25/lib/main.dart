import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ListViewApp(),
    );
  }
}

class ListViewApp extends StatefulWidget {
  const ListViewApp({super.key});

  @override
  State createState() => _ListViewAppState();
}

class _ListViewAppState extends State<ListViewApp> {
  List<List<String>> imageList = [
    [
      "https://www.mykhel.com/img/2023/06/rohit-sharma-out-wtc-1686572734.jpg",
      "https://img1.hscicdn.com/image/upload/f_auto,t_ds_w_1280,q_80/lsci/db/PICTURES/CMS/376200/376277.jpg",
      "https://images.indianexpress.com/2014/12/msdhoni_2.jpg"
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTj1PdtWCDuxmpCgk0POhN4pk5HM_hOeTywaFC4VfWUyw&s",
      "https://images.hindustantimes.com/img/2022/12/10/1600x900/Bangladesh-India-Cricket-58_1670663413177_1670663413177_1670663468996_1670663468996.jpg",
      "https://media.gettyimages.com/id/911936774/photo/christchurch-new-zealand-shubman-gill-of-india-celebrates-his-century-during-the-icc-u19.jpg?s=612x612&w=0&k=20&c=5ejnWuYZqg1rUnrh4QwF1uWVZ8wE_GnR_zklGPCxOco=",
    ],
    [
      "https://e0.pxfuel.com/wallpapers/776/913/desktop-wallpaper-indian-crickter-and-fast-bowler-jasprit-bumrah-in-action.jpg",
      "https://i2.wp.com/wallpapercave.com/wp/wp3990003.jpg?strip=all",
      "https://sc0.blr1.cdn.digitaloceanspaces.com/article/122436-wxtnbhekdo-1561275712.jpg"
    ],
  ];

  List formats = ["Test-PLAYERS ", "T20-PLAYERS ", "ODI-PLAYERS "];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Cricket_Legends"),
        centerTitle: true,
      ),
      body: ListView.separated(
        itemCount: imageList.length,
        separatorBuilder: (context, index) {
          return Container(
            height: 20,
            width: 50,
            child: Text(formats[index]),
          );
        },
        itemBuilder: (BuildContext context, int index) {
          return Column(children: [
            Container(
              height: 600,
              width: 600,
              margin: const EdgeInsets.all(10),
              child: ListView.builder(
                  itemCount: imageList[index].length,
                  itemBuilder: (BuildContext context, int index1) {
                    return Container(
                      height: 200,
                      width: 200,
                      //padding: const EdgeInsets.all(10),
                      margin: const EdgeInsets.all(10),
                      child: Image.network(imageList[index][index1]),
                    );
                  }),
            )
          ]);
        },
      ),
    );
  }
}
