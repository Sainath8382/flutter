import 'package:flutter/material.dart';

class DisplayImages extends StatefulWidget {
  const DisplayImages({super.key});

  @override
  State createState() => _DisplayImagesState();
}

class _DisplayImagesState extends State<DisplayImages> {
  List<String> imageList = [
    "https://i.pinimg.com/originals/a4/c6/45/a4c64516e0786f079a16b5e429f543cc.jpg",
    "https://www.teahub.io/photos/full/170-1703090_2016-nissan-gt-r-wallpaper-hd-gtr-car.jpg",
    "https://wallpapercave.com/wp/wp9013207.jpg",
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("JDM-Gallery",
              style: TextStyle(
                color: Colors.black, /*backgroundColor: Colors.deepPurple*/
              )),
          centerTitle: true,
        ),
        body: ListView.builder(
          itemCount: imageList.length,
          itemBuilder: (context, index) {
            return Container(
                margin: const EdgeInsets.all(10),
                child: Image.network(
                  imageList[index],
                ));
          },
        ),
      ),
    );
  }
}
