import 'package:flutter/material.dart';
import 'package:quiz_app/login.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State createState() => _QuizAppState();
}

class _QuizAppState extends State {
  bool loginscreen = true;

  bool questionScreen = true;
  int questionindex = 0;
  int selectedAnswerindex = -1;
  int correctAnswers = 0;

  List<Map> questions = [
    {
      "Question": "Which city is the Capital of India ?",
      "options": ["Delhi", "Mumbai", "Kolkata", "Pune"],
      "answerindex": 0,
    },
    {
      "Question": "In which year was Flutter launched?",
      "options": ["2000", "2006", "2017", "2019"],
      "answerindex": 2,
    },
    {
      "Question": "Which of the following is an IDE?",
      "options": ["Vim-Editor", "Notepad", "Ubuntu", "VS-Code"],
      "answerindex": 3,
    },
    {
      "Question": "Which language is used in Flutter ?",
      "options": ["C++", "Dart", "Java", "Python"],
      "answerindex": 1,
    },
    {
      "Question": "Which of the following is the virtual machine in Flutter?",
      "options": ["flutter-VM", "JVM", "PVM", "AVF"],
      "answerindex": 0,
    },
  ];

  MaterialStateProperty<Color?> checkAnswer(int btnindex) {
    if (selectedAnswerindex != -1) {
      if (btnindex == questions[questionindex]["answerindex"]) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (btnindex == selectedAnswerindex) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(Colors.white);
      }
    } else {
      return const MaterialStatePropertyAll(Colors.white);
    }
  }

  void validatePage() {
    if (selectedAnswerindex == -1) {
      return;
    }

    if (selectedAnswerindex == questions[questionindex]["answerindex"]) {
      correctAnswers += 1;
    }

    if (selectedAnswerindex != -1) {
      if (questionindex == questions.length - 1) {
        setState(() {
          questionScreen = false;
        });
      }
      selectedAnswerindex = -1;
      setState(() {
        questionindex += 1;
      });
    }
  }

  Scaffold isQuestionScreen() {
    if (questionScreen == true) {
      return Scaffold(
        backgroundColor: Colors.purple.shade100,
        appBar: AppBar(
          backgroundColor: Colors.purple,
          title: const Text(
            "Quiz-APP",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(50),
          child: Column(
            children: [
              const SizedBox(
                height: 25,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                const Text("Questions : ",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    )),
                Center(
                  child: Text(
                    "${questionindex + 1}/${questions.length}",
                    style: const TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ]),
              const SizedBox(
                height: 50,
              ),
              SizedBox(
                width: 420,
                height: 55,
                child: Text(
                  questions[questionindex]["Question"],
                  style: const TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: checkAnswer(0),
                ),
                onPressed: () {
                  if (selectedAnswerindex == -1) {
                    setState(() {
                      selectedAnswerindex = 0;
                    });
                  }
                },
                child: SizedBox(
                  height: 36,
                  width: 300,
                  child: Text(
                    "A.${questions[questionindex]["options"][0]}",
                    style: const TextStyle(
                      color: Colors.black87,
                      fontSize: 25,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 25,
              ),
              ElevatedButton(
                style: ButtonStyle(backgroundColor: checkAnswer(1)),
                onPressed: () {
                  if (selectedAnswerindex == -1) {
                    setState(() {
                      selectedAnswerindex = 1;
                    });
                  }
                },
                child: SizedBox(
                  height: 36,
                  width: 300,
                  child: Text(
                    //textAlign: TextAlign.center,
                    "B.${questions[questionindex]["options"][1]}",
                    style: const TextStyle(
                      color: Colors.black87,
                      fontSize: 25,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 25,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: checkAnswer(2),
                ),
                onPressed: () {
                  if (selectedAnswerindex == -1) {
                    setState(() {
                      selectedAnswerindex = 2;
                    });
                  }
                },
                child: SizedBox(
                  height: 36,
                  width: 300,
                  child: Text(
                    "C.${questions[questionindex]["options"][2]}",
                    style: const TextStyle(
                      color: Colors.black87,
                      fontSize: 25,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 25,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: checkAnswer(3),
                  //elevation: MaterialStateProperty.all(15),
                ),
                onPressed: () {
                  if (selectedAnswerindex == -1) {
                    setState(() {
                      selectedAnswerindex = 3;
                    });
                  }
                },
                child: SizedBox(
                  height: 36,
                  width: 300,
                  child: Text(
                    "D.${questions[questionindex]["options"][3]}",
                    style: const TextStyle(
                      color: Colors.black87,
                      fontSize: 25,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 25,
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              validatePage();
            });
          },
          backgroundColor: Colors.purple,
          child: const Icon(
            Icons.forward_sharp,
            color: Colors.white,
          ),
        ),
      );
    } else {
      return Scaffold(
          backgroundColor: Colors.purple.shade100,
          appBar: AppBar(
            backgroundColor: Colors.purple,
            title: const Text(
              "Quiz-APP",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.all(20),
            child: Expanded(
              child: Column(children: [
                const SizedBox(
                  height: 50,
                ),
                Image.network(
                  "https://as2.ftcdn.net/v2/jpg/05/78/16/57/1000_F_578165709_sRQVO59WF70AkjNw644Mjx55SesD9FYk.jpg",
                  height: 300,
                  width: 400,
                ),
                const Text(
                  "Congratulations!!!",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text("Your QUIZ is Completed!!",
                    style: TextStyle(fontSize: 20)),
                const SizedBox(
                  height: 20,
                ),
                Text("Accuracy: $correctAnswers/${questions.length}",
                    style: const TextStyle(fontSize: 20)),
                const SizedBox(
                  height: 25,
                ),
                ElevatedButton(
                  style: const ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.purple)),
                  onPressed: () {
                    questionindex = 0;
                    selectedAnswerindex = -1;
                    correctAnswers = 0;
                    selectedAnswerindex = -1;
                    setState(() {
                      questionScreen = true;
                    });
                  },
                  child: const Text("RESET",
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                      )),
                )
              ]),
            ),
          ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}
