import 'package:flutter/material.dart';
import 'package:quiz_app/main.dart';

void main() => runApp(const StartPage());

class Start extends StatelessWidget {
  const Start({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: StartPage(),
    );
  }
}

class StartPage extends StatefulWidget {
  const StartPage({super.key});

  @override
  State createState() => _StartPageState();
}

class _StartPageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple,
      body: Column(
        //crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(
            height: 80,
          ),
          const SizedBox(
            width: 29,
          ),
          const Text(
            "Welcome !!",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w400,
              color: Colors.white,
            ),
          ),
          const Text(
            "Sainath",
            style: TextStyle(
              color: Colors.white,
              fontSize: 35,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(
            height: 41,
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(25)),
                color: Colors.purple.shade50,
              ),
              alignment: Alignment.center,
              child: Expanded(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 50,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(25),
                      child: Image.network(
                        "https://wallpaperaccess.com/full/2384093.jpg",
                        height: 180,
                        width: 380,
                        fit: BoxFit.cover,
                      ),
                    ),
                    const SizedBox(
                      height: 35,
                    ),
                    const Text(
                      "Press Start for Starting the quiz",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    ElevatedButton(
                        style: const ButtonStyle(
                          elevation: MaterialStatePropertyAll(20),
                          backgroundColor:
                              MaterialStatePropertyAll(Colors.purple),
                          animationDuration: Duration(seconds: 2),
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const QuizApp()));
                        },
                        child: const Text(
                          "Start",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.w600),
                        ))
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
